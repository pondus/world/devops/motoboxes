#!/usr/bin/env bash

NOMAD_VERSION=1.0.2

set -e

# Update
apt-get update -y
apt-get install curl unzip -y


echo "Installing Consul $NOMAD_VERSION..."
NOMAD_ARCHIVE_HOME=/tmp
NOMAD_ARCHIVE=$NOMAD_ARCHIVE_HOME/nomad.zip
curl --silent --show-error --location \
  https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip > $NOMAD_ARCHIVE

if [[ ! -f /usr/bin/nomad ]]
then
  pushd $NOMAD_ARCHIVE_HOME
  unzip $NOMAD_ARCHIVE
  install nomad /usr/bin/nomad
  popd
  nomad version
  nomad -autocomplete-install
  complete -C /usr/bin/nomad nomad
fi

groupadd --system nomad
useradd -s /sbin/nologin --system -g nomad nomad

mkdir -p /var/lib/nomad
chown -R nomad:nomad /var/lib/nomad
chmod -R 775 /var/lib/nomad

mkdir -p /etc/nomad.d
cp /vagrant/0_server-base.hcl /etc/nomad.d/
chown -R nomad:nomad /etc/nomad.d

cp /vagrant/nomad.service /etc/systemd/system

systemctl enable nomad
systemctl start nomad
